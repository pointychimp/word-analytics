// http://www.reddit.com/r/dailyprogrammer/comments/1e97ob/051313_challenge_125_easy_word_analytics/
// https://bitbucket.org/pointychimp/word-analytics

#include <iostream>
#include <string>
#include <fstream>
#include <vector>
#include <sstream>

using namespace std;

class TextFile
{
    public:
        string file;

        void addWord(string w) {words.push_back(w);}
        void countChar(char c) {characterCount.at((int)c) += 1;}

        unsigned int getNumberWords() {return words.size();}
        unsigned int getNumberChars()
        {
            unsigned int numChars = 0;
            for (unsigned int i = 0; i < characterCount.size(); i++)
            {
                numChars += characterCount.at(i);
            }
            return numChars;
        }
        unsigned int getNumberLetters()
        {
            unsigned int numLetters = 0;
            for (unsigned int i = 'a'; i <= 'z'; i++)
            {
                numLetters += characterCount.at(i);
            }
            return numLetters;
        }
        unsigned int getNumberSymbols()
        {
            unsigned int numSymbols = 0;

            for (unsigned int i = '!'; i <= '/'; i++)
            {
                numSymbols += characterCount.at(i);
            }
            for (unsigned int i = ':'; i <= '@'; i++)
            {
                numSymbols += characterCount.at(i);
            }
            for (unsigned int i = '['; i <= '`'; i++)
            {
                numSymbols += characterCount.at(i);
            }
            for (unsigned int i = '{'; i <= '~'; i++)
            {
                numSymbols += characterCount.at(i);
            }
            return numSymbols;
        }
        string getTopWords(unsigned int minLength = 0)
        {
            vector<unsigned int> wordFrequencies;
            vector<string> foundWords;

            for (unsigned int i = 0; i < words.size(); i++)
            {
                bool unique = true;
                string word = words.at(i);
                for (unsigned int j = 0; j < foundWords.size(); j++)
                {
                    if (word == foundWords.at(j))
                    {
                        unique = false;
                        wordFrequencies.at(j) += 1;
                        break;
                    }
                }
                if (unique && word.length() >= minLength)
                {
                    foundWords.push_back(word);
                    wordFrequencies.push_back(1);
                }
            }
            int first = -1, second = -1, third = -1;
            for (unsigned int i = 0; i < wordFrequencies.size(); i++)
            {
                unsigned int working = wordFrequencies.at(i);
                if (first == -1 || working > wordFrequencies.at(first))
                {
                    third = second;
                    second = first;
                    first = i;
                }
                else if (second == -1 || working > wordFrequencies.at(second))
                {
                    third = second;
                    second = i;
                }
                else if (third == -1 || working > wordFrequencies.at(third))
                {
                    third = i;
                }
                else {}
            }
            string str = foundWords.at(first) + " (" + to_string(wordFrequencies.at(first)) + "), ";
            str += foundWords.at(second) + " (" + to_string(wordFrequencies.at(second)) + "), ";
            str += foundWords.at(third) + " (" + to_string(wordFrequencies.at(third)) + ")";

            return str;
        }
        string getTopLetters()
        {
            int first = -1, second = -1, third = -1;
            for (unsigned int i = 'a'; i <= 'z'; i++)
            {
                if (first == -1 || characterCount.at(i) > characterCount.at(first))
                {
                    third = second;
                    second = first;
                    first = i;
                }
                else if (second == -1 || characterCount.at(i) > characterCount.at(second))
                {
                    third = second;
                    second = i;
                }
                else if (third == -1 || characterCount.at(i) > characterCount.at(third))
                {
                    third = i;
                }
                else {}
            }
            stringstream ss;
            ss << (char)first << " (" << characterCount.at(first) << "), ";
            ss << (char)second << " (" << characterCount.at(second) << "), ";
            ss << (char)third << " (" << characterCount.at(third) << ")";

            return ss.str();
        }
        TextFile() {characterCount.resize(128,0);}
        TextFile(string _file) {file = _file; characterCount.resize(128,0);}
        ~TextFile() {};
    private:


        vector<string> words;
        vector<int> characterCount;
};

int main(int argc, char *argv[])
{
    TextFile currentFile;
    do
    {
        string fileName;
        cout << "This program assumes standard English characters and symbols will be the only input." << endl;

        if (argc == 2) // first arg = program, second = file
        {
            fileName = argv[1];
        }
        else
        {
            cout << "Type \"exit\" at any time to exit" << endl;
            cout << "Path to file to read from: ";
            cin >> fileName;
            if (fileName == "exit") break;
        }

        currentFile = TextFile(fileName);

        ifstream file(currentFile.file.c_str());

        if (file.is_open())
        {
            char c;
            string workingWord;

            while (file >> noskipws >> c)
            {
                c = tolower(c);
                currentFile.countChar(c);
                if (isspace(c))
                {
                    if (workingWord.length() > 0) currentFile.addWord(workingWord);
                    workingWord = "";
                }
                else if ('!' <= c && c <= '/' ||
                          ':' <= c && c <= '@' ||
                          '[' <= c && c <= '`' ||
                          '{' <= c && c <= '~'  )
                {
                    if (workingWord.length() > 0) currentFile.addWord(workingWord);
                    workingWord = "";
                }
                else
                {
                    workingWord += c;
                }
            }
            if (workingWord.length() > 0) currentFile.addWord(workingWord);
        }
        else
        {
            cout << "Couldn't open file." << endl;
            return 1;
        }

        if (currentFile.getNumberWords() < 3)
        {
            cout << "File too short. Contains less than three words." << endl;
            return 0;
        }

        cout << "----------\n";
        cout << "# of words: " << currentFile.getNumberWords() << endl;
        cout << "# of letters: " << currentFile.getNumberLetters() << endl;
        cout << "# of symbols: " << currentFile.getNumberSymbols() << endl;
        cout << "top 3 words: " << currentFile.getTopWords() << endl;
        cout << "top 3 words \"long\" words: " << currentFile.getTopWords(4) << endl;
        cout << "top 3 letters: " << currentFile.getTopLetters() << endl;
        cout << "----------\n\n";

    } while (0);
    return 0;
}

